# Console-Calculator-Application
The project code was written following the video-call of pair programming of Anatoliy Kolodkin and Ruslan Opishniyak.

The concepts of Mock Tests was introduced, however, IMHO, it needs more consideration to understand this.


## QA01 Group 7
Project was updated to dotnet 8 and added a couple of tests.
Coverlet was added to the project to review the unit test coverage.

### How to execute
Execute Unit Tests with the following command:
```
dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=cobertura
```

Generate Report command:
```
reportgenerator -reports:"**\coverage.cobertura.xml" -targetdir:"coveragereport"-reporttypes:Html
```
The HTML report will be under folder coveragereport.
